# encoding: utf-8

require 'capybara/rspec'
require 'capybara/poltergeist'

require 'phantomjs' # <-- Not required if your app does Bundler.require automatically (e.g. when using Rails)
Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app, :phantomjs => Phantomjs.path)
end

Capybara.default_driver    = :poltergeist
Capybara.javascript_driver = :poltergeist

describe "Form filling", :type => :feature do
    before :each do
        visit 'http://sportsmaps.herokuapp.com/'
    end
    
    it "With data, to submit the form" do
        fill_in 'sports_map[localization]', :with => 'Latitude: -23.558745 Longitude: -46.731859'
        fill_in 'sports_map[radius]', :with => '5'
        check('sports_map[air-quality]')
        check('sports_map[bike-paths]')
        uncheck('sports_map[bike-paths]')
        check('sports_map[ultraviolet]')
        check('sports_map[humidity]')
        check('sports_map[temperature]')
        check('sports_map[green-areas]')
        uncheck('sports_map[green-areas]')
        click_on('Submit')
    end
end

